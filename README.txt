/*************************************/
 Ubercart eTranzact
/************************************/

Author: Kayode Odeyemi (drecute)

Requires: Ubercart

Description
-----------
Ubercart eTransact is a contrib module for Ubercart for eTranzact WebConnect.

From http://goo.gl/5TzAC, it reads, eTranzact WebConnect is a solution developed to accept and 
process payment on behalf of the merchant. The solution is ported or integrated on eTranzact 
merchant website.

eTranzact WebConnect API is unique because of its capability to accept all cards issued locally and international, cards like Visa and Master Cards. It has the ability to credit multiple account simultaneously online real-time..

This solution can be integrated on all Websites irrespective of the programming language used for 
development.

The transaction data are encrypted and transmitted to the eTranzact central switch for 
authentication and validation. 

Please submit issues you have with the module in the issue queue.

This module is sponsored by http://datasphir.com and http://opevel.com.

